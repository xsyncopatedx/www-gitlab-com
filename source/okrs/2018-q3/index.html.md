---
layout: markdown_page
title: "2018 Q3 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. IACV at 120% of plan, pipeline for Q4 at 3x IACV minus in quarter orders, LTV/CAC per campaign and type of customer.

* VPE
  * Support
    * Self-hosted: X% on CSAT, X% on Premium SLAs
    * Services: X% on CSAT, X% on Premium SLAs

### CEO: Popular next generation product. Complete DevOps GA, GitLab.com ready for mission critical applications, graph of DevOps score vs. cycle time.

* VPE
  * Frontend: Implement 10 performance improvements: X/10 (X%)
  * Dev Backend
    * Discussion: Implement 10 performance improvements: X/10 (X%)
    * Distribution: Implement 10 performance improvements: X/10 (X%)
    * Geo: Implement 10 performance improvements: X/10 (X%)
    * Gitaly: Implement 10 performance improvements: X/10 (X%)
    * Gitter: Implement 10 performance improvements: X/10 (X%)
    * Platform: Implement 10 performance improvements: X/10 (X%)
  * Ops Backend
    * CI/CD: Implement 10 performance improvements: X/10 (X%)
    * Configuration: Implement 10 performance improvements: X/10 (X%)
    * Monitoring: Implement 10 performance improvements: X/10 (X%)
    * Security Products: Implement 10 performance improvements: X/10 (X%)
* VPE: Make GitLab.com ready for mission-critical customer workloads (99.95% availability)
  * Frontend: 100% uptime according to TBD budget
  * Dev Backend
    * Discussion: 100% uptime according to TBD budget
    * Distribution: 100% uptime according to TBD budget
    * Geo: 100% uptime according to TBD budget
    * Gitaly: 100% uptime according to TBD budget
    * Gitter: 100% uptime according to TBD budget
    * Platform: 100% uptime according to TBD budget
  * Infrastructure
    * Database: 100% uptime according to TBD budget
    * Production: 100% uptime according to TBD budget
  * Ops Backend
    * CI/CD: 100% uptime according to TBD budget
    * Configuration: 100% uptime according to TBD budget
    * Monitoring: 100% uptime according to TBD budget
    * Security Products: 100% uptime according to TBD budget
  * UX
    * Establish the User Experience direction for the security dashboard: Aid in completing a Competitive analysis, identify 10 must have items for the security dashboard, complete a design artifact/discovery issue.
    * UX Research
      * Incorporate personas into our design process for 100% of our product features.
      * Identify 5 pain points for users who have left GitLab.com. Work with Product Managers to identify solutions.

### CEO: Great team. ELO score per interviewer, Employer branding person, Real-time dashboard for all Key Results.

* VPE: 10 iterations to engineering function handbook: X/10 (X%)
  * Frontend: 10 iterations to frontend department handbook: X/10 (X%)
  * Infrastructure: 10 iterations to infrastructure department handbook: X/10 (X%)
    * Production: 10 iterations to production team handbook: X/10 (X%)
  * Ops Backend: 10 iterations to ops backend department handbook: X/10 (X%)
    * CI/CD: 10 iterations to CI/CD team handbook: X/10 (X%)
    * Configuration: 10 iterations to configuration team handbook: X/10 (X%)
    * Monitoring: 10 iterations to monitoring team handbook: X/10 (X%)
    * Security Products: 10 iterations to security products team handbook: X/10 (X%)
  * Quality: 10 iterations to quality department handbook: X/10 (X%)
  * Security: 10 iterations to security department handbook: X/10 (X%)
  * Support: 10 iterations to support department handbook: X/10 (X%)
      * Self-hosted: 10 iterations to self-hosted team handbook: X/10 (X%)
      * Services: 10 iterations to services team handbook: X/10 (X%)
* VPE: Source 300 candidates for various roles: X sourced (X%)
  * Frontend: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Dev Backend: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Discussion: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Distribution: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Geo: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Gitaly: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Gitter: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Platform: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Infrastructure: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Database: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Production: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Ops Backend: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * CI/CD: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Configuration: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Monitoring: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Security Products: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Quality: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Security: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * Support: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Self-hosted: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
    * Services: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
  * UX: Source X candidates by July 15 and hire X: X sourced (X%), hired X (X%)
