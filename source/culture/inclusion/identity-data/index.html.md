---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-06-12.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 292   | 100%        |
| Based in the US                           | 167  |  57.19%      |
| Based in the UK                           | 20    | 6.85%     |
| Based in the Netherlands                  | 11    | 3.77%       |
| Based in Other Countries                  | 93    | 31.85%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 292   | 100%        |
| Men                                       | 230   | 78.77%      |
| Women                                     | 60    | 20.55%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 28    | 100%        |
| Men in Leadership                         | 23    | 82.14%      |
| Women in Leadership                       | 5     | 17.86%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 121   | 100%        |
| Men in Development                        | 108   | 89.26%      |
| Women in Development                      | 13    | 10.74%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 167   | 100%        |
| Asian                                     | 12    | 7.19%       |
| Black or African American                 | 3     | 1.80%       |
| Hispanic or Latino                        | 12    | 7.19%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.60%       |
| Two or More Races                         | 5     | 2.99%       |
| White                                     | 88    | 52.69%      |
| Unreported                                | 46    | 27.54%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 38    | 100%        |
| Asian                                     | 4     | 10.53%      |
| Black or African American                 | 1     | 2.63%       |
| Hispanic or Latino                          3       7.89%
  Two or More Races                         | 2     | 5.26%       |
| White                                     | 18    | 47.37%      |
| Unreported                                | 10    | 26.32%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 23    | 100%        |
| Asian                                     | 2     | 8.70%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.35%       |
| White                                     | 13    | 52.17%      |
| Unreported                                | 8     | 34.78%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 292   | 100%        |
| Asian                                     | 22    | 7.53%       |
| Black or African American                 | 6     | 2.05%       |
| Hispanic or Latino                        | 19    | 6.51%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.34%       |
| Two or More Races                         | 6     | 2.05%       |
| White                                     | 151   | 51.71%      |
| Unreported                                | 87    | 29.79%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 121   | 100%        |
| Asian                                     | 9     | 7.44%       |
| Black or African American                 | 3     | 2.48%       |
| Hispanic or Latino                        | 9     | 7.44%       |
| Two or More Races                         | 3     | 2.48%       |
| White                                     | 59    | 48.76%      |
| Unreported                                | 38    | 31.40%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 28    | 100%        |
| Asian                                     | 2     | 7.14%       |
| Hispanic or Latino                          1       3.57%
  Native Hawaiian or Other Pacific Islander | 1     | 3.57%       |
| White                                     | 13    | 46.43%      |
| Unreported                                | 11    | 39.29%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 292   | 100%        |
| 18-24                                     | 18    | 6.16%       |
| 25-29                                     | 70    | 23.97%      |
| 30-34                                     | 82    | 28.08%      |
| 35-39                                     | 44    | 15.07%      |
| 40-49                                     | 49    | 16.78%      |
| 50-59                                     | 25    | 8.56%       |
| 60+                                       | 2     | 0.68%       |
| Unreported                                | 2     | 0.68%       |
